import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';




//定义一个模型
class Model{
  final String title;
  final String description;
  //模型的构造方法
  Model(this.title, this.description);
}



void main(){
  runApp(
    MaterialApp(
      title: 'ListDemo',
      //通过模型数组创建列表页
      home: ListScreen(
          modelArray:List.generate(20, (i)=>Model('操作$i','第$i个页面的内容详细描述'))
      ),
    )
  );
}

//列表页
class ListScreen extends StatelessWidget {
  final List<Model> modelArray;
  //构造方法(根据模型数组构建)
  ListScreen({Key key,@required this.modelArray}):super(key:key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListDemo'),
      ),
      body:ListView.builder(itemBuilder: (context,index){
        //相当于返回cell
        return ListTile(
          title: Text(modelArray[index].title),
          //相当于cell上的点击事件
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context)=>DetailScreen(
                model: modelArray[index],
              )),
            );
          },
        );
      },
        itemCount: modelArray.length,
      )
    );
  }
}

class DetailScreen extends StatelessWidget {
  final Model model;
  DetailScreen({Key key,@required this.model}):super(key:key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(model.title),
      ),
      body: Center(
        child: Text(model.description),
      ),
    );
  }
}



